import sys
from lifxlan import LifxLAN
from apscheduler.schedulers.background import BlockingScheduler

lifx = LifxLAN()

light = lifx.get_device_by_name('Living')

# *_color(color, [duration ms], [rapid boolean])
# color: [hue (0-65535), sat (0-65535), bright (0-65535), Kelvin (2500-9000)]
def wakeup():
    light.set_color((0, 0, 0, 3000))
    light.set_power("on")
    light.set_color((0, 0, 65535, 9000), 30000)

(hour,minute) = sys.argv[1].split(':')

scheduler = BlockingScheduler()
scheduler.add_job(wakeup, 'cron', hour=hour, minute=minute)
scheduler.start()
