# wakeup

An intro to python and manipulating the world

Note: You may need to install packages for this. Many packages found online will
say to install via "pip" like `pip install <package>`. pip is the default way to
install things but conda makes it a little easier to make separate runtime
environments. In other words, sometimes a project doesn't need all the packages
that another one does or maybe they need the same package but different versions.
conda makes managing those situations easy, but sometimes conda doesn't have
access to all the packages that pip does. In those cases, you'll need to install
pip in your environment and use it instead.

1. Press windows key and type "Anaconda prompt" to open a command prompt for
        your anaconda environment. Prompt should read `(base) C:\Users\<user>`
2. `conda install pip`
3. `pip install <packagesyouneed>`

From the anaconda prompt you may also run python and other commands.

And now for a general process to build a scheduled light:

1. web search for python library to use lifx api [1]
    1. how to get a specific light into a variable?
    2. how to manipulate a light once captured in a variable?
2. web search for how to schedule events in python [2][3]
    1. try every 10 seconds
    2. try at a specific time (a few minutes ahead of now)
3. web search for how to read arguments (like '7:10') [4]
    1. learn how to parse the argument (we want minutes and hours separate)
        e.g. convert arg string to date or time and extract hour/minute?
        or split [5] the arg string on the ":" ?

future thoughts:
    how to input the time from a physical device?
        a modified clock face would be effectively two dials, but how to get the
            value of those dials into the schedule?
        other input methods could be GUI like a website
        
[1]https://github.com/mclarkk/lifxlan

[2]https://apscheduler.readthedocs.io/en/latest/py-modindex.html

[3]https://stackoverflow.com/questions/22715086/scheduling-python-script-to-run-every-hour-accurately/22715345#22715345

[4] https://docs.python.org/3.6/tutorial/stdlib.html#command-line-arguments

[5]https://docs.python.org/3.6/library/stdtypes.html#str.split

